using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace AzureTesting.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public string Greeting => "Welcome to Avalonia!";
        [Reactive] public int No1 { get; set; }
        [Reactive] public int No2 { get; set; }
        [Reactive] public int No3 { get; set; }

        public MainWindowViewModel()
        {
            this.WhenAnyValue(x => x.No1)
                .Subscribe(_ => No3 = No1 + No2);
            this.WhenAnyValue(x => x.No2)
                .Subscribe(_ => No3 = No1 + No2);        
        }
    }
}
